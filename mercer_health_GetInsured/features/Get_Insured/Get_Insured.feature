Feature: Get Insured Smoke Test cases

  @Smoke1 @GI1
  Scenario Outline: Verify the Footer links for Gated Ungated Portal
    Given User opens the getInsured URl "<GetInsuredURL>"
    When User should able to verify the footer Paragraphs
    Examples:
      | GetInsuredURL                                                |

      #Gated Portal URL's
      | https://retireehealth.premedicareplans.com/subsidy           |
      | https://Ahlstrom.premedicareplans.com/subsidy                |
      | https://Enbridge.premedicareplans.com/subsidy                |
      | https://Momentive.premedicareplans.com/subsidy               |
      | https://Motorolasolutions.premedicareplans.com/subsidy       |
      | https://TransCanada.premedicareplans.com/subsidy             |
#
      #Ungated Portal URL's
      | https://RetireeHealthAccess.premedicareplans.com/accessonly  |
      | https://AECOM.premedicareplans.com/accessonly                |
      | https://Albemarle.premedicareplans.com/accessonly            |
      | https://Devon.premedicareplans.com/accessonly                |
      | https://Retiree.premedicareplans.com/accessonly              |
      | https://Quest.premedicareplans.com/accessonly                |
      | https://Silgan.premedicareplans.com/accessonly               |
      | https://TCaccess.premedicareplans.com/accessonly             |
      | https://URS.premedicareplans.com/accessonly                  |
      | https://MSIaccess.premedicareplans.com/accessonly            |


  @Smoke1 @GI2
  Scenario Outline: Verify the default page for Gated Ungated Portal
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User able to verify the tollfree number "<TollFree_Number>"
    And User able to Verify Contact Us header of that page
    And User able to navigate to hompage after clicking Home Button
    And User able to validate help link headers of Landing Pages
    And User should able to verify the login page of "<GetInsuredURL>" Portals
    Then User should able to verify terms and Condition and Privacy Policy link
    Examples:
      | GetInsuredURL                                                |TollFree_Number|

      #Gated Portal URL's
      | https://retireehealth.premedicareplans.com/subsidy           |866-609-4810   |
      | https://Ahlstrom.premedicareplans.com/subsidy                |844-618-6285   |
      | https://Enbridge.premedicareplans.com/subsidy                |888-264-9246   |
      | https://Momentive.premedicareplans.com/subsidy               |888-264-9323   |
      | https://Motorolasolutions.premedicareplans.com/subsidy       |844-851-5426   |
      | https://TransCanada.premedicareplans.com/subsidy             |844-618-6289   |
#
      #Ungated Portal URL's
      | https://RetireeHealthAccess.premedicareplans.com/accessonly  |866-696-8685   |
      | https://AECOM.premedicareplans.com/accessonly                |877-975-3275   |
      | https://Albemarle.premedicareplans.com/accessonly            |866-658-9898   |
      | https://Devon.premedicareplans.com/accessonly                |844-213-9959   |
      | https://Retiree.premedicareplans.com/accessonly              |800-752-7049   |
      | https://Quest.premedicareplans.com/accessonly                |888-264-9876   |
      | https://Silgan.premedicareplans.com/accessonly               |800-685-6350   |
      | https://TCaccess.premedicareplans.com/accessonly             |866-609-4803   |
      | https://URS.premedicareplans.com/accessonly                  |800-709-7696   |
      | https://MSIaccess.premedicareplans.com/accessonly            |866-867-6897   |


  @Smoke1 @GI3
  Scenario Outline: Verify the gated Portal On and Off Exchange flow
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User clicks on Get Started Link and login with "<FirstName>" "<LastName>" and "<SSN>"
    And User able to login Successfully in Gated portal and Shop for Header Insurance Page should be displayed
    And User able to Complete the Off Exchange flow for gated portal

    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User clicks on Get Started Link and login with "<FirstName>" "<LastName>" and "<SSN>"
    And User able to login Successfully in Gated portal and Shop for Header Insurance Page should be displayed
    And  User able to complete the On Exchange flow for Gated Portal

    Examples:
      | GetInsuredURL                                                |FirstName  |LastName    |SSN|

      #Gated Portal URL's
      | https://retireehealth.premedicareplans.com/subsidy           |One        |Retiree     |3000|
      | https://Ahlstrom.premedicareplans.com/subsidy                |Tester     |Ahlstrom    |1000|
      | https://Enbridge.premedicareplans.com/subsidy                |Tester     |Enbridge    |1000|
      | https://Momentive.premedicareplans.com/subsidy               |Tester     |Momentive   |1000|
      | https://Motorolasolutions.premedicareplans.com/subsidy       |Tester     |Motorola    |1000|
      | https://TransCanada.premedicareplans.com/subsidy             |Tester     |Transcanada |1000|


  @Smoke1 @GI4
  Scenario Outline: Verify the Non-gated Portal Flow On and Off Exchange flow
    Given User opens the getInsured URl "<GetInsuredURL>"
    When User able to login Successfully in Non-Gated portal and Shop for Header Insurance Page should be displayed
    Then  User able to Complete the Off Exchange flow for Non-Gated portal

    Given User opens the getInsured URl "<GetInsuredURL>"
    When User able to login Successfully in Non-Gated portal and Shop for Header Insurance Page should be displayed
    Then  User able to Complete the On Exchange flow for Non-Gated portal


    Examples:
      | GetInsuredURL |
      | https://RetireeHealthAccess.premedicareplans.com/accessonly  |
      | https://AECOM.premedicareplans.com/accessonly                |
      | https://Albemarle.premedicareplans.com/accessonly            |
      | https://Devon.premedicareplans.com/accessonly                |
      | https://Retiree.premedicareplans.com/accessonly              |
      | https://Quest.premedicareplans.com/accessonly                |
      | https://Silgan.premedicareplans.com/accessonly               |
      | https://TCaccess.premedicareplans.com/accessonly             |
      | https://URS.premedicareplans.com/accessonly                  |
      | https://MSIaccess.premedicareplans.com/accessonly            |


  @Smoke1 @GI5
  Scenario Outline: Verify the product links for Gated Portal
    Given User opens the getInsured URl "<GetInsuredURL>"
    When  User clicks on Get Started Link and login with "<FirstName>" "<LastName>" and "<SSN>"
    And   User able to click and verify Health Product Link for Gated Portal
    And   User clicks on Get Started Link and login with "<FirstName>" "<LastName>" and "<SSN>"
    And   User able to click and verify Medicare Product Link for Gated Portal
    And   User able to click and verify Dental Product Link for Gated Portal
    And   User able to click and verify Vision Product Link for Gated Portal
    Then  User able to verify Other Insurance Product link in Motorola Clients "<GetInsuredURL>" for Gated Portal

    Examples:
      | GetInsuredURL                                                |FirstName  |LastName    |SSN|

      #Gated Portal URL's
      | https://retireehealth.premedicareplans.com/subsidy           |One        |Retiree     |3000|
      | https://Ahlstrom.premedicareplans.com/subsidy                |Tester     |Ahlstrom    |1000|
      | https://Enbridge.premedicareplans.com/subsidy                |Tester     |Enbridge    |1000|
      | https://Momentive.premedicareplans.com/subsidy               |Tester     |Momentive   |1000|
      | https://Motorolasolutions.premedicareplans.com/subsidy       |Tester     |Motorola    |1000|
      | https://TransCanada.premedicareplans.com/subsidy             |Tester     |Transcanada |1000|


  @Smoke1 @GI6
  Scenario Outline: Verify the product links for Non-Gated Portal
    Given User opens the getInsured URl "<GetInsuredURL>"
    When User able to login Successfully in Non-Gated portal and Shop for Header Insurance Page should be displayed
    And   User able to click and verify Health Product Link for Non-Gated Portal
    When User able to login Successfully in Non-Gated portal and Shop for Header Insurance Page should be displayed
    And   User able to click and verify Medicare Product Link for Non-Gated Portal
    And   User able to click and verify Dental Product Link for Non-Gated Portal
    And   User able to click and verify Vision Product Link for Non-Gated Portal

    Examples:
      | GetInsuredURL                                                |
      | https://RetireeHealthAccess.premedicareplans.com/accessonly  |
      | https://AECOM.premedicareplans.com/accessonly                |
      | https://Albemarle.premedicareplans.com/accessonly            |
      | https://Devon.premedicareplans.com/accessonly                |
      | https://Retiree.premedicareplans.com/accessonly              |
      | https://Quest.premedicareplans.com/accessonly                |
      | https://Silgan.premedicareplans.com/accessonly               |
      | https://TCaccess.premedicareplans.com/accessonly             |
      | https://URS.premedicareplans.com/accessonly                  |
      | https://MSIaccess.premedicareplans.com/accessonly            |

