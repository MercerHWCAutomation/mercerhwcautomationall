﻿module.exports = {
    sections: {

        //VIPConfig locators
        VIPPage: {
            selector: 'body',
            elements: {
                pageTitle: {locateStrategy: 'xpath', selector: '//a[contains(text(),"BENEFITS")]'},
                tableClientHomePage: { selector: '#js-client-list-table'},

                linkQAACME: {locateStrategy: 'xpath', selector: '//a[contains(text(),"QAACME")]'},
                linkMMC365: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Mercer Marketplace 365 Plus")]'},

                tabCarrierList: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Carrier ID List")]'},
                tabPremReport: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Prem Report Config")]'},
                tabCustomFieldMapping: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Custom Field Mapping")]'},
                tabReportSettings: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Report Settings")])[1]'},
                tabCarrierReportConfigurations: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Carrier Report Configurations")]'},

                titleQAACME: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"QAACME")]'},
                titleMMC365: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Mercer Marketplace 365 Plus")]'},
                titleTabCarrierList: {locateStrategy: 'xpath', selector: '//h3[contains(text(),"Carrier List")]'},
                titleTabPremReport: {locateStrategy: 'xpath', selector: '//h3[contains(text(),"Premium Report Configuration")]'},

                viewActiveInactive: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Active")]/../..'},
                viewActive: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Active")]'},
                viewInactive: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Inactive")]'},

                columnnCarrierID:{locateStrategy: 'xpath', selector: '(//a[contains(text(),"Carrier ID")])[2]'},
                columnnCarrierDesc:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Carrier Description")]'},
                columnnEffectiveDate:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Effective Date")]'},
                columnnLastUpdated:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Last Updated")]'},
                columnnSuppressCarriers:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Suppress Carriers")]'},
                columnnSuppressPremiums:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Suppress Premiums")]'},
                columnnNumber:{locateStrategy: 'xpath', selector: '//a[contains(text(),"#")]'},
                columnnFieldDisplayName:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Field Display Name")]'},
                columnnTable:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Table")]'},
                columnnFieldNames:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Field Name")]'},
                columnnCarrierDisplayName:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Carrier Display Name")]'},
                columnnCarrierBenefitIds:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Benefit IDs")]'},
                columnnActions:{locateStrategy: 'xpath', selector: '//th[contains(text(),"Actions")]'},
                columnnActionsCarrierReportsTab:{locateStrategy: 'xpath', selector: '(//th[contains(text(),"Actions")])[2]'},

                inputBoxFilter: {locateStrategy: 'xpath', selector: '//input[@id="address-filter"]/../..'},

                labelCustomFieldMapping: {locateStrategy: 'xpath', selector: '//h4[contains(text(),"Custom Field Mapping")]'},
                labelReportSettings: {locateStrategy: 'xpath', selector: '//h4[contains(text(),"Report Settings")]'},
                labelCarrierReportConfigurations: {locateStrategy: 'xpath', selector: '//h4[contains(text(),"Carrier Report Configurations")]'},

                fieldMarkSSN: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Mask SSN:")]/..'},
                fieldPremReportLive: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Premium Report Live:")]/..'},
                fieldClientSummaryKeyField: {locateStrategy: 'xpath', selector: '//label[contains(text(),"Client Summary Key Field:")]/..'},


            }
        },

    }
};



