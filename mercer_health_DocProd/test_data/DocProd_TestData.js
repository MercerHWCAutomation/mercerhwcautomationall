module.exports = {

    TestingEnvironment: 'QAI',
    DcoProd_ExecStatus:'Y',
   // TestingEnvironment: 'CITEST',
   // TestingEnvironment: 'CSO',
   // TestingEnvironment: 'PROD',

    // NameofThebrowser: "internet explorer",
    NameofThebrowser: "chrome",

    SelectPlanName1 : "DB",
    SelectPlanName2 : "HB",
    InterviewNowflow : "YES",

    shortpause: 3000,
    averagepause: 11000,
    longpause: 35000,
    shortwaittime: 17000,
    averagewaittime: 50000,
    longwaittime: 80000,

    urlQAI: 'https://afcore-qai.mercer.com/home.tpz',
    urlCITEST: 'https://afcore-ctesti.mercer.com/home.tpz',
    urlCSO: 'https://afcore-csoi.mercer.com/home.tpz',
    urlPROD: 'https://af-us.mercer.com/home.tpz',

    // QAI Environment - Test data
     SSN : "901000070",  // HB AND DB

    // CITEST Environment - Test data
    // SSN : 888880472,  // HB

    // CSO Environment - Test data
    //   SSN : "777700054",  // HB AND DB

    // PROD Environment - Test data
    //   SSN : "777190001",  // HB AND DB

    // QAI - Interview Now Flow
     SSNInterviewnow : "000000002",   // HB
     FirstName: "FNAME",
     LastName: "LNAME0001",
     ClientName: "KAIS15",

};
