Feature: Regression Validations in ESR Application

  ######################################### QA01CL  #######################################
  @QA01CL @monthly
  Scenario Outline: User downloads the monthy reports and compares the excels for QA01CL
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientQA01CL" Link or Button
    #When     The user navigates to specified "QA01CL" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    Then  User checks rerun for all scenarios
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "QA01CL" for "<Report>"
    Then    User moves to the specifed folder "Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "QA01CL" for "<Report>"
    Then    User moves to the specifed folder "QA01CL_ONG_<Report>"
    Then     User downloads "NHR" for the client "QA01CL" for "<Report>"
    Then    User moves to the specifed folder "QA01CL_NHR_<Report>"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2015    |2014|Oct  |Monthly_OCT_2014|
      |2016    |2014|Dec  |Monthly_DEC_2014|
      |2016    |2015|May  |Monthly_MAY_2015|
      |2016    |2015|Oct  |Monthly_OCT_2015|
      |2016    |2015|Dec  |Monthly_DEC_2015|

  @QA01CL @annual
  Scenario: User downloads the Annual reports and compares the excels for QA01CL
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientQA01CL" Link or Button
   # When     The user navigates to specified "QA01CL" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
   # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2015" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
   # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "QA01CL" for "2015"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_QA01CL_2015"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

  @QA01CL @monthly
  Scenario Outline: User downloads the monthy reports and compares the excels for QA01CL
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientQA01CL" Link or Button
    #When     The user navigates to specified "QA01CL" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    Then  User checks rerun for all scenarios
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "QA01CL" for "<Report>"
    Then    User moves to the specifed folder "Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "QA01CL" for "<Report>"
    Then    User moves to the specifed folder "QA01CL_ONG_<Report>"
    Then     User downloads "NHR" for the client "QA01CL" for "<Report>"
    Then    User moves to the specifed folder "QA01CL_NHR_<Report>"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2017    |2016|Oct  |Monthly_OCT_2016|
      |2017    |2016|Dec  |Monthly_DEC_2016|

  @QA01CL @annual
  Scenario: User downloads the Annual reports and compares the excels for QA01CL
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientQA01CL" Link or Button
   # When     The user navigates to specified "QA01CL" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
   # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2016" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
   # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "QA01CL" for "2016"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_QA01CL_2016"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

 ######################################### SLSDM ##########################################


  @SLSDM @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
   # When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "ONG" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_ONG_<Report>"
    Then     User downloads "NHR" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
    |IRS_Year|Year|Month|Report|
    |2016    |2014|Nov  |Monthly_NOV_2014|
    |2016    |2014|Dec  |Monthly_DEC_2014|
    |2016    |2015|Jan  |Monthly_JAN_2015|
    |2016    |2015|Feb  |Monthly_FEB_2015|


    @SLSDM @monthly
  Scenario Outline: User downloads the monthy reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
      Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
    #When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
    #Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "ONG" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_ONG_<Report>"
    Then     User downloads "NHR" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2016    |2015|Mar  |Monthly_MAR_2015|
      |2016    |2015|Apr  |Monthly_APR_2015|
      |2016    |2015|May  |Monthly_MAY_2015|
      |2016    |2015|Jun  |Monthly_JUNE_2015|


  @SLSDM @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
   # When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "ONG" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_ONG_<Report>"
    Then     User downloads "NHR" for the client "SLSDM" for "<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button
    Then    User moves to the specifed folder "SLSDM_NHR_<Report>"

    Examples:
      |IRS_Year|Year|Month|Report|
      |2016    |2015|Jul  |Monthly_JULY_2015|
      |2016    |2015|Aug  |Monthly_AUG_2015|
      |2016    |2015|Sep  |Monthly_SEPT_2015|
      |2016    |2015|Oct  |Monthly_OCT_2015|



  @SLSDM @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
    #When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "ONG" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_ONG_<Report>"
    Then     User downloads "NHR" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2017    |2015|Nov  |Monthly_NOV_2015|
      |2017    |2015|Dec  |Monthly_DEC_2015|
      |2017    |2016|Jan  |Monthly_JAN_2016|
      |2017    |2016|Feb  |Monthly_FEB_2016|


  @SLSDM @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
   # When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "ONG" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_ONG_<Report>"
    Then     User downloads "NHR" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2017    |2016|Mar  |Monthly_MAR_2016|
      |2017    |2016|Apr  |Monthly_APR_2016|
      |2017    |2016|May  |Monthly_MAY_2016|
      |2017    |2016|Jun  |Monthly_JUNE_2016|


  @SLSDM @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
   # When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    #Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "ONG" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_ONG_<Report>"
    Then     User downloads "NHR" for the client "SLSDM" for "<Report>"
    Then    User moves to the specifed folder "SLSDM_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2017    |2016|Jul  |Monthly_JULY_2016|
      |2017    |2016|Aug  |Monthly_AUG_2016|
      |2017    |2016|Sep  |Monthly_SEPT_2016|
      |2017    |2016|Oct  |Monthly_OCT_2016|




  @SLSDM @annual
  Scenario: User downloads the Annual reports and compares the excels for SLSDM
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientSLS" Link or Button
   # When     The user navigates to specified "SLSDM" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
   # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2016" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
   # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "SLSDM" for "2016"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_SLSDM_2016"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

    ############################### YCCTST  ########################

  @YCCTST @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for YCLIFEPYR COBRA TEST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
    #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    Then     User should see "ESRHomePage|titleMonthlyReportPopUp" message is displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCCTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_ONG_<Report>"
    Then     User downloads "NHR" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
       |2015    |2015|Jan  |Monthly_JAN_2015|
       |2015    |2015|Feb  |Monthly_FEB_2015|
       |2015    |2015|Mar  |Monthly_MAR_2015|
       |2015    |2015|Apr  |Monthly_APR_2015|
       |2015    |2015|May  |Monthly_MAY_2015|





  @YCCTST @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for YCLIFEPYR COBRA TEST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
    #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    Then     User should see "ESRHomePage|titleMonthlyReportPopUp" message is displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
# Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCCTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_ONG_<Report>"
    Then     User downloads "NHR" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2015    |2015|Jun  |Monthly_JUNE_2015|
      |2015    |2015|Jul  |Monthly_JULY_2015|
      |2015    |2015|Aug  |Monthly_AUG_2015|
      |2015    |2015|Sep  |Monthly_SEPT_2015|
      |2015    |2015|Oct  |Monthly_OCT_2015|

  @YCCTST @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for YCLIFEPYR COBRA TEST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
    #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    Then     User should see "ESRHomePage|titleMonthlyReportPopUp" message is displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
# Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCCTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_ONG_<Report>"
    Then     User downloads "NHR" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2016    |2015|Nov  |Monthly_NOV_2015|
      |2016    |2015|Dec  |Monthly_DEC_2015|
      |2016    |2016|Jan  |Monthly_JAN_2016|
      |2016    |2016|Feb  |Monthly_FEB_2016|
      |2016    |2016|Mar  |Monthly_MAR_2016|


  @YCCTST @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for YCLIFEPYR COBRA TEST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
    #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    Then     User should see "ESRHomePage|titleMonthlyReportPopUp" message is displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
# Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCCTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_ONG_<Report>"
    Then     User downloads "NHR" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2016    |2016|Apr  |Monthly_APR_2016|
      |2016    |2016|May  |Monthly_MAY_2016|
      |2016    |2016|Jun  |Monthly_JUNE_2016|
      |2016    |2016|Jul  |Monthly_JULY_2016|
      |2016    |2016|Aug  |Monthly_AUG_2016|

  @YCCTST @monthly

  Scenario Outline: User downloads the monthy reports and compares the excels for YCLIFEPYR COBRA TEST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
   # When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    Then     User should see "ESRHomePage|titleMonthlyReportPopUp" message is displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
# Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCCTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_ONG_<Report>"
    Then     User downloads "NHR" for the client "YCCTST" for "<Report>"
    Then    User moves to the specifed folder "YCLIFEPYR_COBRA_TEST_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2016    |2016|Sep  |Monthly_SEPT_2016|
      |2016    |2016|Oct  |Monthly_OCT_2016|
      |2017    |2016|Nov  |Monthly_NOV_2016|
      |2017    |2016|Dec  |Monthly_DEC_2016|


  @YCCTST @annual
  Scenario: User downloads the Annual reports and compares the excels for YCCTST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
    #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
    #Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2015" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
   # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "YCCTST" for "2015"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_YCCTST_2015"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

  @YCCTST @annual
  Scenario: User downloads the Annual reports and compares the excels for YCCTST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
    #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
   # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2016" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
    #Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "YCCTST" for "2016"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_YCCTST_2016"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

    #########################################  VCTC01   #########################################3

  @VCTC01 @monthly

    Scenario Outline: User downloads the monthy reports and compares the excels for Valley_Crest_Companies

    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientVCTC01" Link or Button
    #When     The user navigates to specified "Valley Crest Companies" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    #Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
    #Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
   # Then     User downloads "Reconciliation" for the client "VCTC01" for "<Report>"
   # Then    User moves to the specifed folder "VCTC01_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "VCTC01" for "<Report>"
    Then    User moves to the specifed folder "Valley_Crest_Companies_ONG_<Report>"
    Then     User downloads "NHR" for the client "VCTC01" for "<Report>"
    Then    User moves to the specifed folder "Valley_Crest_Companies_NHR_<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2015    |2014|Apr  |Monthly_APR_2014|
      |2015    |2014|May  |Monthly_MAY_2014|
      |2015    |2014|Jun  |Monthly_JUNE_2014|
      |2015    |2014|Jul  |Monthly_JULY_2014|
      |2015    |2014|Aug  |Monthly_AUG_2014|
      |2015    |2014|Sep  |Monthly_SEPT_2014|
      |2015    |2014|Oct  |Monthly_OCT_2014|
      |2015    |2014|Nov  |Monthly_NOV_2014|
      |2015    |2014|Dec  |Monthly_DEC_2014|
      |2015    |2015|Jan  |Monthly_JAN_2015|
      |2015    |2015|Feb  |Monthly_FEB_2015|
      |2015    |2015|Mar  |Monthly_MAR_2015|
      |2016    |2015|Apr  |Monthly_APR_2015|
      |2016    |2015|May  |Monthly_MAY_2015|
      |2016    |2015|Jun  |Monthly_JUNE_2015|














  ################################################ ESRTST ###################################

  @ESRTST @monthly

  Scenario: User downloads the monthy reports and compares the excels for ESRTST

    Given    Once User is logged in to ESR application
    When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2015", year "2014" and month "Dec"
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "ESRTST" for "<Report>"
    Then    User moves to the specifed folder "ESRTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "ESRTST" for "Monthly_DEC_2014"
    Then    User moves to the specifed folder "ESRTST_ONG_Monthly_DEC_2014"
    Then     User downloads "NHR" for the client "ESRTST" for "Monthly_DEC_2014"
    Then    User moves to the specifed folder "ESRTST_NHR_Monthly_DEC_2014"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

# # @ESRTST @ant        ####### dropdown doesnt contain 2014 and Functional QA specifed to ignore
 # Scenario: User downloads the Annual reports and compares the excels for ESRTST


  @ESRTST @monthly

  Scenario: User downloads the monthy reports and compares the excels for ESRTST

    Given    Once User is logged in to ESR application
    When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2016", year "2015" and month "Dec"
    Then     Confirmation message should be displayed successfully for Monthly report
    #Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "ESRTST" for "<Report>"
    Then    User moves to the specifed folder "ESRTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "ESRTST" for "Monthly_DEC_2015"
    Then    User moves to the specifed folder "ESRTST_ONG_Monthly_DEC_2015"
    Then     User downloads "NHR" for the client "ESRTST" for "Monthly_DEC_2015"
    Then    User moves to the specifed folder "ESRTST_NHR_Monthly_DEC_2015"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

  @ESRTST @annual
  Scenario: User downloads the Annual reports and compares the excels for ESRTST
    Given    Once User is logged in to ESR application
    When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
    #Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2015" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
    #Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "ESRTST" for "2015"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_ESRTST_2015"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button

  @ESRTST @monthly

  Scenario: User downloads the monthy reports and compares the excels for ESRTST

    Given    Once User is logged in to ESR application

    When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
   # Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2017", year "2016" and month "Dec"
    Then     Confirmation message should be displayed successfully for Monthly report
    #Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "ESRTST" for "<Report>"
    Then    User moves to the specifed folder "ESRTST_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "ESRTST" for "Monthly_DEC_2016"
    Then    User moves to the specifed folder "ESRTST_ONG_Monthly_DEC_2016"
    Then     User downloads "NHR" for the client "ESRTST" for "Monthly_DEC_2016"
    Then    User moves to the specifed folder "ESRTST_NHR_Monthly_DEC_2016"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

  @ESRTST @annual
  Scenario: User downloads the Annual reports and compares the excels for ESRTST
    Given    Once User is logged in to ESR application
    When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
   # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2016" for monthly report
    Then     Confirmation message should be displayed successfully for Annual report
   # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed

    Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
    Then     User downloads annual report for the client "ESRTST" for "2016"
    Then     User moves to the specifed folder "ANNUAL ESR REPORT_ESRTST_2016"
    When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button
   # Given User executes query for serv_end_dt

  @FlatFileESRTST
  Scenario: Flat file comparison for ESRTST client

  #Given User lists the differences for "ANNUAL ESR REPORT_QA01CL_2016112017_1513846388059"


    Given      Once User is logged in to ESR application
    When     The user navigates to specified "ESRTST" Page
    When     User Clicks on "ESRHomePage|Reports" Link or Button
    Then     User Clicks on "ESRHomePage|AnnualReports" Link or Button
    Then     User Clicks on "ESRHomePage|GenerateFlatFile" Link or Button
   # Then     "ESRHomePage|confirmationMessage" page or message should be displayed
    Given     User lists the differences for "ANNUAL ESR REPORT_ESRTST_2015_12222017_015452"

##################################### Simple scenarios    #############################################

  @ESR @SC94
  Scenario: Validation for Space in Member Identifier
    Given Once User is logged in to ESR application
    When The user navigates to specified "QA01CL" Page
    When User Navigates to Reporting years tab
    When User selects the particular year on Reporting years page
    When User adds a new member
    Then User should be thrown an error
    #Then User should be able to see Member and and their identifying details
    # And Enter space on the Member Identifier field
#Then User should be on Reporting years page
   # When User edits the same field with all special characters
   # Then User should be thrown an error for special char

  @ESR @SC74
  Scenario: Validation for increase in column length for MedicalPlanName

    Given    Once User is logged in to ESR application
    When     The user navigates to specified "QA01CL" Page
   # When     User Clicks on "ESRHomePage|buttonBrowseFile" Link or Button
    Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
    And      User uploads the inbound file having increase in column length for Medical plan name
    Given    User navigates to Data_Files Tab
    Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
    #Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
    And      User generates the annual report
    #Then     "ESRHomePage|messageSuccess" page or message should be displayed

  @ESR @SC75
    Scenario: Validation having LCMP Plan start and end dates equal

      Given    Once User is logged in to ESR application
      When     The user navigates to specified "QA01CL" Page
      Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
      And      User uploads the inbound file having LCMP Plan start and end dates equal
      Then User verifies for upload status

      #When     User Clicks on "ESRHomePage|buttonBrowseFile" Link or Button


     # Given    User navigates to Data_Files Tab
     # Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
     # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
     # And      User generates the annual report
     # Then     "ESRHomePage|messageSuccess" page or message should be displayed

#################################### Monthly Report comparison on Resave ##############################
@mc
  Scenario: Monthly Report comparison on Resave

    Given User lists the differences for "Valley_Crest_Companies_NHR_Monthly_MAY_2014112017_1513846358525"

#################################### Annual Report Comparison on Resave  ##############################


  @ac1
  Scenario: Flat file comparison for CHILDR client

  Given      Once User is logged in to ESR application
  When     The user navigates to specified "Children's Hospital of Philadelphia" Page
  When     User Clicks on "ESRHomePage|Reports" Link or Button
  Then     User Clicks on "ESRHomePage|AnnualReports" Link or Button
  Then     User Clicks on "ESRHomePage|GenerateFlatFile" Link or Button
  #Then     "ESRHomePage|confirmationMessage" page or message should be displayed
  Given     User lists the differences for "ANNUAL ESR REPORT_CHILDR_2015_12222017_085620"






    ###########################  ******* NOT Done*******

      @ESR @SC89
      Scenario: Validate when repeated correction files are uploaded for five times with changes in newly added fields
       # Given    "ESRHomePage|linkUploadFile" page or message should be displayed
       # And      "ESRHomePage|linkUploadBWFile" page or message should be displayed
        When     User Clicks on "ESRHomePage|linkUploadBWFile" Link or Button
       # Then     "ESRHomePage|titleUploadBWFilePopUp" page or message should be displayed
        When     User uploads the BW file

##############################################CHILDR################################################
  @CHILDR2015
    Scenario: Monthly report generation of CHILDR_2015
    Given    Once User is logged in to ESR application
    When     The user navigates to specified "Children's Hospital of Philadelphia" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    #Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2015", year "2015" and month "Apr"
    Then  User checks rerun for all scenarios
    #Then     "ESRHomePage|buttonOKMonthlyReportPopUp" page or message should be displayed

    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "CHILDR" for "Monthly_APR_2015"
    Then    User moves to the specifed folder "Children's Hospital of Philadelphia_Reconcilation_Monthly_APR_2015"
    Then     User downloads "ONG" for the client "Children's Hospital of Philadelphia" for "Monthly_APR_2015"
    Then    User moves to the specifed folder "Children's Hospital of Philadelphia_ONG_Monthly_APR_2015"
    Then     User downloads "NHR" for the client "Children's Hospital of Philadelphia" for "Monthly_APR_2015"
    Then    User moves to the specifed folder "Children's Hospital of Philadelphia_NHR_Monthly_APR_2015"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

  @CHILDR2016
  Scenario: Monthly report generation of CHILDR_2016
    Given    Once User is logged in to ESR application
    When     The user navigates to specified "Children's Hospital of Philadelphia" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    #Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
    And      The User selects the IRS year "2016", year "2016" and month "Apr"
    Then  User checks rerun for all scenarios
    #Then     "ESRHomePage|buttonOKMonthlyReportPopUp" page or message should be displayed
    Then     Confirmation message should be displayed successfully for Monthly report
   # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then    User moves to the specifed folder "Children's Hospital of Philadelphia_Reconcilation_<Report>"
    Then     User downloads "ONG" for the client "Children's Hospital of Philadelphia" for "Monthly_APR_2016"
    Then     User downloads "Reconciliation" for the client "Children's Hospital of Philadelphia" for "<Report>"
    Then    User moves to the specifed folder "Children's Hospital of Philadelphia_ONG_Monthly_APR_2016"
    Then     User downloads "NHR" for the client "Children's Hospital of Philadelphia" for "Monthly_APR_2016"
    Then    User moves to the specifed folder "Children's Hospital of Philadelphia_NHR_Monthly_APR_2016"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button


    #---------------------------------------------TEST---------------------------------------------------

  @abcde @monthly
  Scenario Outline: User downloads the monthy reports and compares the excels for YCLIFEPYR COBRA TEST
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientYCCTST" Link or Button
   #When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
    Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
    Then     User should see "ESRHomePage|titleMonthlyReportPopUp" message is displayed
    And      The User selects the IRS year "<IRS_Year>", year "<Year>" and month "<Month>"
    Then     Confirmation message should be displayed successfully for Monthly report
  # Then     "ESRHomePage|messageSuccessMonthlyReport" page or message should be displayed
    Then     User downloads "Reconciliation" for the client "YCCTST" for "<Report>"
    Then    User moves to the Pre folder "<Report>"
    Then    User moves to the Post folder "<Report>"
    Then     User downloads "ONG" for the client "YCCTST" for "<Report>"
    Then    User moves to the Pre folder "<Report>"
    Then    User moves to the Post folder "<Report>"
    Then     User downloads "NHR" for the client "YCCTST" for "<Report>"
    Then    User moves to the Pre folder "<Report>"
    Then    User moves to the Post folder "<Report>"
    When     User Clicks on "ESRHomePage|tabDataFiles" Link or Button

    Examples:
      |IRS_Year|Year|Month|Report|
      |2015    |2015|Jan  |Monthly_JAN_2015|
      |2015    |2015|Feb  |Monthly_FEB_2015|
      #|2015    |2015|Mar  |Monthly_MAR_2015|
      #|2015    |2015|Apr  |Monthly_APR_2015|
      #|2015    |2015|May  |Monthly_MAY_2015|

