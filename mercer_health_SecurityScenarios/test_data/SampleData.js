/**
 * Created by Mohsin on 12/12/2017.
 * Updated by Rachana on 03/01/2018
 */





module.exports = {
    TestingEnvironment:'QAI',
    //TestingEnvironment: 'SmallMarket',
    //TestingEnvironment: 'LargeMarket',
    shortWait: 5000,
    longWait: 10000,
    LoginWait: 30000,
    urlMBCQA3: 'https://auth-qai.mercerbenefitscentral.com/mbcqa3/login.tpz',
    urlSmallMarket: 'https://ben-qaf3.mercerbenefitscentral.com/login',
    urlLargeMarket: 'https://ben-qaf3.mercerbenefitscentral.com/?clientid=FNCLIENTC&employeeid=117301055',
    urlDevf: 'https://ben-dev3.mercerbenefitscentral.com/?clientid=DVCLIENTC&employeeid=117301004',
    urlDocumentLibrary:'https://consultant-qaf.mercermarketplace365plus.com/#/documents/',
    LifeEventStartDate: '04/04/2017',
    FirstnameAddDep: 'PQR',
    LastnameAddDep: 'XYZ',
    DOBAddDep: '03/03/1993',
    SSNAddDep: '989654123',
    BrowserInTest: 'chrome',
    //BrowserInTest: 'internet-explorer',


    UserMBCQA3: {
        MBCUser: {
            username: 'testmbc33194', password: 'Test0001' },
    },
    UserSmallMarket: {
        MBCUser: {
            usernameSM: 'Copy.Dress10@airfake.com', passwordSM: 'Test0001!' },
    },


};
