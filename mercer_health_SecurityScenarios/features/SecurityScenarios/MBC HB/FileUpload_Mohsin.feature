Feature: FileUploading

  @MBCSecurity @Fileupload
  Scenario: To validate file upload
    Given User goes to dependent verification file upload
    When User uploads an unexpected file
    Then Verify error message for unexpected file
    And User logs out from MBC Application

#Then User shouldnt see the content of the file in the browse