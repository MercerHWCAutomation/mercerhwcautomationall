Feature: HRA Xeos Smoke Test cases

  @Security @HRAXeosBypassingAuthorizationSchema @1
  Scenario:To verify that a normal user does not have access to the SSO enabled page
    Given User opens the SSO Url

  @Security @EncryptedCredentials @2
  Scenario: To verify that User credentials are in encrypted format
    Given   User opens the browser and launch the HRAXeos Portal URl
    When    User Login to the HRA XEOS application using valid Internal User Login credentials
    Then    User data are transported in encrypted format

  @Security @pptLogin @4
  Scenario: To verify that a ppt is not able to access the file/page of the application before login
    Given User opens the browser and launch the HRAXeos Portal URl
    When User enters a different url

  @Security @ErrorHandling @5
    Scenario: To verify the error message after making invalid attempt on Internal user login page.
    Given User opens the browser and launch the HRAXeos Portal URl
    When User enters incorrect login credentials
    Then User must see message Either Username or Password incorrect

  @Security @SQLInjection @12
    Scenario: To verify that the application does not disclose information when SQL Queries are injected in The URL of the application
    Given User opens the browser and launch the HRAXeos Portal URl
    When User enters sql url
    And User must see error message

  @Security @ClickJacking @6
    Scenario: To verfiy whether Internal User Login page opens in an iframe
    Given User open the html file in any browser
    Then check whether the site is vulnerale or not

  @Security @StrictHTTP @7
  Scenario: To verify security of Internal User Login of HRA XEOS application
    Given User opens the browser and launch the HRAXeos Portal URl
    When User changes https to http
    Then User must be redirected to actual URL

  @Security @ErrorMessages @10
    Scenario: To verify that the HRA XEOS application does not present application error messages to an attacker that could be used in an attack
    Given User opens the browser and launch the HRAXeos Portal URl
    When User enters valid credentials
    Then User must recieve error message

  @Security @SessionTimeout @8
  Scenario: To verify session timeout in Internal User Login of HRA XEOS application
    Given User opens the browser and launch the HRAXeos Portal URl
    When User logs in with valid credentials and does not use for specified time limit
    Then Application has automatically logged you out


  @Security @SensitiveData @9
    Scenario: To verify that the HRA XEOS application does not remember sensitive data
    Given User opens the browser and launch the HRAXeos Portal URl
    When User clicks on username field
    Then browser must not prompt with recently used usernames

  @Security @UploadFile @10
    Scenario: To verify that the application does not allow the upload of unexpected file types
    Given User opens the browser and launch the HRAXeos Portal URl
    When User Login to the HRA XEOS application using valid credentials
    And User navigates to claim page
    And User clicks on Create ont time claim
    And User enters all the details and clicks on the upload button
    Then User gets a error message after uploading a .exe file


  @Security @ResetPassword @11
  Scenario: To verify the Password reset and remember password functionality
    Given User opens the browser and launch the HRAXeos Portal URl
    When User enters incorrect login credential
    And User clicks on forgot password
    Then User requests for a new password


  @Security @LockedOut @3
  Scenario: To verify that user gets locked out after making invalid attempts
    Given   User opens the browser and launch the HRAXeos Portal URl
    When try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    And try to login with incorrect username or password
    Then User enters valid credentials



















