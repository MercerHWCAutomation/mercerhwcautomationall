var data = require('../../TestResources/GlobalTestData_Rachana.js');
var Objects = require(__dirname + '/../../repository/MBCHBPages_Rachana.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');

var LoginPage;

function initializePageObjects(browser, callback) {
    var MBCPage1 = browser.page.MBCHBPages_Rachana();
    LoginPage = MBCPage1.section.LoginPage;
    callback();
}
module.exports = function() {

    this.Given(/^User launches the MBC Application in Browser$/, function () {
        var URL;
        var execEnv = data["TestingEnvironment"];
        execEnv=== "QAI";
        console.log('Test Environment: ' + execEnv);
        browser = this;
        URL = data.URLQAI;
        initializePageObjects(browser, function () {
                        browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(15000);
            LoginPage.waitForElementVisible('@Username', data.wait);
        });
    });

    this.When(/^User enters the username in Username textbox$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Username', data.wait)
            .setValue('@Username', data.Username);
        browser.pause(15000);

    });
//Query

    this.When(/^User enters the SQL Query as password in the Password textbox$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Password', data.wait)
            .setValue('@Password', data.SQLPassword);
        browser.pause(data.wait);
    });

    this.When(/^User enters the password in the Password textbox$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Password', data.wait)
            .setValue('@Password', data.Password);
        browser.pause(data.wait);
    });

    this.When(/^User clicks on submit$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@SubmitButton', data.wait)
            .click('@SubmitButton')
    });

    this.Then(/^Verify error message for wrong Username and Password$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@SubmitButton', data.wait)
        browser.useXpath().assert.containsText("//img[@src='/Images/blank.gif']", "");
        this.pause(5000)
    });

    this.When(/^User clicks on Menu$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@MenuButton', data.wait)
        LoginPage.click('@MenuButton')
        this.pause(45000)
    });

    this.When(/^User clicks on Complete Life Event$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@CompleteLifeEventButton', data.wait)
            .click('@CompleteLifeEventButton')
    });

    this.When(/^User clicks on Get Started Tab$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@GetStartedTab', data.wait)
            .click('@GetStartedTab')
    });

    this.When(/^User navigates to Who's covered page$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@WhosCoveredTab', data.wait)
            .click('@WhosCoveredTab')
        this.pause(5000);
    });

    this.Then(/^User clicks on Add Dependent Button$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@AddDependentButton', data.wait)
            .click('@AddDependentButton')
        this.pause(5000);
    });

    this.Then(/^User clicks on Continue Button$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@ContinueButton', data.wait)
            .click('@ContinueButton')
        this.pause(5000);
    });

    this.Then(/^User enters the SQL Query in SSN textfield$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@SSN', data.wait)
            .setValue('@SSN', data.SSN).click('@Random')
        this.pause(5000);
    });

    this.Then(/^Verify error message when entered wrong SSN$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Random', data.wait)
        browser.useXpath().assert.containsText("//div[.='Please enter a valid SSN.']", "Please enter a valid SS");
        this.pause(5000);
    });

    this.Then(/^User logs out of MBC Application$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@LogoutButton', data.wait)
            .click('@LogoutButton')
        this.pause(5000);
    });

    this.Then(/^User navigates to My Information page$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@ContinueButtoninWCP', data.wait)
            .click('@ContinueButtoninWCP');
        this.pause(5000);
    });

    this.Then(/^User clicks on edit button MyInfoPage$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@EditButtoninWCP', data.wait)
            .click('@EditButtoninWCP');
        this.pause(5000);
    });

    this.Then(/^User enters SQL query in Mobile phone text field$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Mobiletextfield', data.wait)
            .setValue('@Mobiletextfield', data.SSN).click('@Random1')
        this.pause(5000);
    });

    this.Then(/^Verify error message for entering wrong Mobile number$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Random1', data.wait)
        browser.useXpath().assert.containsText("//div[.='Please enter a valid phone number.']", "");
        this.pause(5000);
    });
    this.When(/^User clicks on Profile link$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Profilepage', data.wait)
            .click('@Profilepage')
        this.pause(5000);
    });
    this.When(/^User clicks on edit button in Profile page$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@EditbuttoninProfilepage', data.wait)
            .click('@EditbuttoninProfilepage')
        this.pause(5000);
    });

    this.Then(/^User enters the data in the editable field$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Addressfield', data.wait)
            .setValue('@Addressfield', data. Alertdata).click('@Random3')
        this.pause(5000);
    });

    this.Then(/^Verifies the alert popup$/, function () {
        browser = this;
        LoginPage.waitForElementVisible('@Random3', data.wait)
        browser.useXpath().assert.containsText("((//div[@name='content[fieldName]'])[6]//div)[3]", "You've exceeded the maximum characters allowed.");
        this.pause(5000);
    });
}

