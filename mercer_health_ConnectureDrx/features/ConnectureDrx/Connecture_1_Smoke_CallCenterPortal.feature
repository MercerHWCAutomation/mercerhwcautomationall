Feature: Smoke Scenarios for CallCenterPortal

  @Connecture @CallCenterPortal @t1
  Scenario: Navigate to CallCenter Portal
    Given User Navigate to CallCenter Portal Using "CALLCENTERUSER" Successfully

  @Connecture @CallCenterPortal @SearchProfilefunctionality
  Scenario: Search Profile and Enrollments
     When User Enters Firstname, Lastname and Clicks on Search button
     Then Verify Search Results should be displayed
     Then Export Search Results in CSV format
     Then Export Search Results in TXT format
     When User Clicks on Enroll History in front of any Retiree
     Then Verify that Link to Shop and Compare Plans Will be displayed
     When User Clicks on Shop and Compare plans link
     Then Verify that Health Information page loads successfully
     When Verify that Zipcode is Pre-Populated and Clicks on Continue button
     Then Verify that Medicare Extra Help page loads successfully
     When User Selects Level of qualification for Extra help and Clicks on Continue button
     Then Verify that Add drugs page loads successfully
     When Search a few drugs in the search box and Clicks on Find Drug button
     When User Select a drug dosage and quantity and click on Add Drug
     When User Clicks on Drug List is Complete button
     Then Verify that Add Pharmacy page loads successfully
     When User Clicks on Skip button
     Then Verify that View and Compare Plans Page loads successfully
     And  Verify Medicare Advantage Prescription Drug Plans and Prescription Drug Plans
     And  Verify Medicare Advantage Plans and Medicare Supplement plans are displayed
     When User Clicks on View Details button
     Then Verify that Plan Details loads successfully

  @Connecture @CallCenterPortal @AgentAccountManagement
  Scenario: Search for Agents
    When User Clicks on Agent Account Management link
    Then Verify that Agent Account Management page loads successfully
    When User Enters Agent Firstname,Lastname,Username and Clicks on Search Agent accounts button
    Then Verify that Agent Firstname,Lastname,Username and Status displayed in the agent search results

  @Connecture @CallCenterPortal @SignOutfunctionality
  Scenario: SignOut Functionality
    When User Clicks on SignOut link
    Then Verify User should be able to logout successfully






