Feature: Smoke Scenario for CurrentCoverage

@IBC @Smoke @CurrentCoverage
  Scenario:  Verify Current Coverage link in Health Menu
    Given User is logged in using "IBCUSER" to IBC application
    Then Verify Health Tab displayed in Home Page
    Then Verify Current Coverages Link displayed in Health Menu
