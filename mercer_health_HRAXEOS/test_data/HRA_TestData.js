module.exports = {

    TestingEnvironment: 'QA',
    HRAXeos_ExecStatus:'Y',
    //TestingEnvironment: 'SANDBOX',
    //TestingEnvironment: 'STRESS',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',

    NameofThebrowser: "chrome",

    miniWait: 2000,
    shortWait: 5000,
    longWait: 10000,


    url_QA: 'https://qa-claims.mercermarketplace.com/index.php/member/member_main',
    url_QA_Internal:   'https://qa-claims.mercermarketplace.com/mercer_login',


    usersQA: {
        PartcipantUser: {
            username: '833941311', password: 'Test@123' },

        InternalUser: {
            username: 'me2', password: 'mee' },
    },
};


