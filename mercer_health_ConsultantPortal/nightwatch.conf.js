var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data = require('./test_data/Afweb_TestData.js');
sauce_labs_username = "suresh-byregowda";
sauce_labs_access_key = "3ef3f035-78b6-4bd8-b676-364de0ef45fd";

require('nightwatch-cucumber')({
   // supportFiles: ['../utils/TestExecListener.js'],
    stepTimeout: 140000,
    defaultTimeoutInterval: 40000,
    nightwatchClientAsParameter: true
});

module.exports = {
    //AppName: 'AfWeb',
    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "objects",
	globals_path: "../GlobalTestData.js",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: 5555,
        platform: 'Windows 10',
        cli_args: {
            "webdriver.chrome.driver" :chromedriver.path,
            'webdriver.ie.driver': '../IEDriverServer.exe',
            'webdriver.firefox.driver': firefoxDriver.path
        }
    },
    test_settings: {
        default: {
            launch_url: 'http://localhost',
            page_objects_path: 'objects',
            selenium_host: '127.0.0.1',
            selenium_port: 5555,
            silent: true,
            disable_colors: false,
            screenshots: {
                enabled: true,
                on_failure: true,
                on_error: true,
                path: 'screenshots'
            },
            globals: {
	            appName: "" +
                "afweb"
            },
            desiredCapabilities: {
                browserName: data.NameofThebrowser,
                // browserName: "internet explorer",
                javascriptEnabled: true,
                acceptSslCerts: true
            },
        },

        SauceLabs: {
            launch_url: 'http://ondemand.saucelabs.com:80',
            selenium_port: 80,
            selenium_host: 'ondemand.saucelabs.com',
            silent: true,
            username: sauce_labs_username,
            access_key: sauce_labs_access_key,
            screenshots: {
                enabled: false,
                path: '',
            },
            globals: {
                waitForConditionTimeout: 10000,
            },
            desiredCapabilities: {
                name:'Afweb Test',
                browserName: 'microsoftedge',
                platform: 'Windows 10',
                version: '14',
                parentTunnel:'erik-horrell',
                tunnelIdentifier:'MercerTunnel2',
            }
        },
        microsoftedge: {
            desiredCapabilities: {
                browserName: 'microsoftedge'
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts : true
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ie: {
            desiredCapabilities: {
                browserName: 'internet explorer',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        // Appium Config details
        iphone : {
            launch_url : "http://" + sauce_labs_username + ":" + sauce_labs_access_key +
            "@ondemand.saucelabs.com:80/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "iphone",
                platformName: "iOS",
                deviceName: "iPhone Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPHONE EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ipad : {
            launch_url : "http://127.0.0.1:4723/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "ipad",
                platformName: "iOS",
                deviceName: "iPad Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPAD EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        android : {
            launch_url : "http://localhost:5554/wd/hub",
            selenium_port  : 5554,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "android",
                platformName: "ANDROID",
                deviceName: "",
                version: "",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        }

    }

}


