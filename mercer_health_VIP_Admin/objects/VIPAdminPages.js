/**
 * Created by subhajit-chakraborty on 9/12/2017.
 */

module.exports = {
    sections: {
        ClientListPage: {
            selector: 'body',
            elements: {

                ClientListHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Client List')]"},
                BenefitsHubHeader: {locateStrategy: 'xpath',selector: "//span[contains(text(),'HUB')]"},
                FirstClientListName: {selector: "tbody tr:nth-child(2) td a"},
                FilterTabBenHubPage: {locateStrategy: 'xpath',selector: "//input[@id='client-list-filter']"},
            }
        },
    }
};
